# FOSS4G Workshop

## Prerequisites
  - [WSL](https://learn.microsoft.com/en-us/windows/wsl/install)
  - [postgresql](https://sbp.enterprisedb.com/getfile.jsp?fileid=1258514) with postgis enabled
  - [QGIS](https://qgis.org/downloads/QGIS-OSGeo4W-3.32.0-1.msi)
  - [postgrest](https://postgrest.org/en/stable/explanations/install.html)
```sh
wget https://github.com/PostgREST/postgrest/releases/download/v11.1.0/postgrest-v11.1.0-linux-static-x64.tar.xz
tar -xf postgrest-v11.1.0-linux-static-x64.tar.xz
sudo cp postgrest /usr/local/bin/
```
  - [nvm](https://github.com/nvm-sh/nvm#installing-and-updating)
    - nvm i node

## Getting started
  - [rog](https://gitlab.com/nibioopensource/resolve-overlap-and-gap/-/blob/workshop_Foss4G_2023/src/test/sql/regress/workshop_foss4g/create_db.txt)
  - [topology_update](https://gitlab.com/nibioopensource/topology_update_app/-/blob/main/README.md)
